class Artist:
     artist = "Художник"
     def __init__(self, name, country):
         self.name = name
         self.country = country
     def __str__(self):
          return f"{self.name} из {self.country}"

MShagal = Artist("Марк Шагал","Витебская область, Беларусь")
Mane = Artist("Эдуард Мане","Париж")
print(MShagal)
print(Mane)
