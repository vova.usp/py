import time

def timer(function):
    def wrapped(*args):
        start_time = time.perf_counter() * 1000
        res = function(*args)
        work_time = (time.perf_counter() * 1000) - start_time
        print(f"Время выполнения func: {work_time:.4f} сек.")
        return res
    return wrapped

@timer
def func(x, y):
    return x + y
func(10, 20)
